import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Steps {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User launch AUT")
	def userLaunch() {
		Mobile.startApplication('C:\\Users\\ikhsan.ginting\\Documents\\Project\\Retro.0.13.14.apk',false)
	}

	@When("User input (.*) and (.*)")
	def inputCredentials(String email, String password) {
		Mobile.setText(findTestObject('android.widget.EditText0 - Masukkan Email'), email, 0)
		Mobile.setText(findTestObject('android.widget.EditText0 - Masukkan Password'), password, 0)
		Mobile.tap(findTestObject('android.widget.Button0 - MASUK'), 0)
	}

	@Then("User is directed to homescreen")
	def verifyHomescreen() {
		Mobile.waitForElementPresent(findTestObject('Explore/android.widget.TextView0 - Tes Fisik'), 0)
		Mobile.getText(findTestObject('android.widget.Button0 - Lihat Detail'), 0)
	}

	@Then("User failed to login")
	def verifyError(){
		Mobile.checkElement(findTestObject('android.widget.TextView0 - Emailpassword salah'), 0)
	}

	@Then("User check profile")
	def checkProfile(){
		Mobile.tap(findTestObject('Explore/android.widget.ImageView0'), 0)
		Mobile.waitForElementPresent(findTestObject('Explore/android.widget.ImageView - Profile Pic'), 0)
		Mobile.takeScreenshot('C:\\Users\\ikhsan.ginting\\Documents\\Project\\retroKatalon\\Profile.png')
		Mobile.tap(findTestObject('Explore/android.widget.ImageView0 logout'), 0)
	}

	@Then("User check Tes Fisik (.*)")
	def checkFisik(String status){
		Mobile.waitForElementPresent(findTestObject('Explore/android.widget.TextView0 - Tes Fisik'), 0)
		Mobile.getText(findTestObject('Explore/android.widget.TextView0 - Tes Lab Status'), 1)
		Mobile.verifyElementText(findTestObject('Explore/android.widget.TextView0 - Tes Fisik Status'), status)
		Mobile.tap(findTestObject('android.widget.Button0 - Lihat Detail'), 0)
		Mobile.pressBack()
	}

	@Then("User check Tes Lab (.*)")
	def checkLab(String status){
		Mobile.waitForElementPresent(findTestObject('Explore/android.widget.TextView0 - Tes Fisik'), 0)
		Mobile.swipe(750, 700, 0, 700)
		Mobile.clearText(findTestObject('Explore/android.widget.TextView0 - Tes Lab Status'), 0)
		Mobile.getText(findTestObject('Explore/android.widget.TextView0 - Tes Lab'), 0)
		Mobile.getText(findTestObject('Explore/android.widget.TextView0 - Tes Lab Status'), 1)
		Mobile.verifyElementText(findTestObject('Explore/android.widget.TextView0 - Tes Lab Status'), status)
		Mobile.tap(findTestObject('android.widget.Button0 - Lihat Detail'), 0)
		Mobile.swipe(750, 1100, 750, 10)
		Mobile.pressBack()
	}

	@Then("User check Tes Wawancara (.*)")
	def checkInterview(String status){
		Mobile.waitForElementPresent(findTestObject('Explore/android.widget.TextView0 - Tes Fisik'), 0)
		Mobile.swipe(750, 700, 0, 700)
		Mobile.swipe(750, 700, 0, 700)
		Mobile.getText(findTestObject('Explore/android.widget.TextView0 - Tes Wawancara'), 0)
		Mobile.getText(findTestObject('Explore/android.widget.TextView0 - Tes Wawancara Status'), 1)
		Mobile.verifyElementText(findTestObject('Explore/android.widget.TextView0 - Tes Wawancara Status'), status)
		Mobile.tap(findTestObject('android.widget.Button0 - Lihat Detail'), 0)
		Mobile.swipe(750, 1100, 750, 10)
		Mobile.pressBack()
	}

	@Then("User check Penawaran Kontrak (.*)")
	def checkContract(status){
		Mobile.waitForElementPresent(findTestObject('Explore/android.widget.TextView0 - Tes Fisik'), 0)
		Mobile.swipe(750, 700, 0, 700)
		Mobile.swipe(750, 700, 0, 700)
		Mobile.swipe(750, 700, 0, 700)
		Mobile.getText(findTestObject('Explore/android.widget.TextView0 - Penawaran Kontrak'), 0)
		Mobile.getText(findTestObject('Explore/android.widget.TextView0 - Penawaran Kontrak Status'), 1)
		Mobile.verifyElementText(findTestObject('Explore/android.widget.TextView0 - Penawaran Kontrak Status'), status)
		Mobile.tap(findTestObject('android.widget.Button0 - Lihat Detail'), 0)
		Mobile.swipe(750, 1100, 750, 10)
		Mobile.pressBack()
	}
}