@Explore
Feature: Check status Retro App

  Scenario Outline: Check Status
    Given User launch AUT
    When User input <email> and <password>
    Then User is directed to homescreen
    Then User check Tes Fisik <result_1>
    Then User check Tes Lab <result_2>
    Then User check Tes Wawancara <result_3>
		Then User check Penawaran Kontrak <result_4>
		Then User check profile
    Examples: 
      | email                        | password |result_1   |result_2        |result_3        |result_4        |
      | cakrajiya.roberto.36@mail.com| 123456Ad |Lolos      |Lolos           |Lolos           |Lolos           |