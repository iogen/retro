@Login
Feature: Login Retro App

  @Positive
  Scenario Outline: Positive Login
    Given User launch AUT
    When User input <email> and <password>
    Then User is directed to homescreen
    Then User check profile

    Examples: 
      | email                        | password |
      | cakrajiya.roberto.36@mail.com| 123456Ad |
      | CAKRAJIYA.ROBERTO.36@mail.com| 123456Ad |
      | Cakrajiya.Roberto.36@mail.com| 123456Ad |
      
  @Negative
  Scenario Outline: Negative Login
    Given User launch AUT
    When User input <wrong_email> and <wrong_password>
    Then User failed to login

    Examples: 
      | wrong_email        | wrong_password |
      | fernando@gmail.org | 12345678       |
      | FERNANDO@gmail.mom | 12345678       |