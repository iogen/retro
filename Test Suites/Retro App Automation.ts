<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Retro App Automation</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e81056f8-538a-48bb-af4f-e1433be68b4b</testSuiteGuid>
   <testCaseLink>
      <guid>c7a3495e-5f81-42eb-a3a6-468e878c1ef5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab0c8165-c9b7-4ed4-9f62-2c4d3564ea4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
