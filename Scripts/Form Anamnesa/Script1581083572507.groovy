import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('C:\\Users\\ikhsan.ginting\\Documents\\exercise-katalon-appium\\Apk\\Retro Application_id.co.iconpln.retroapplication.apk', 
    true)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - Masukkan Email'), 'cakrajiya.roberto.36@mail.com', 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - Masukkan Password'), '12345678', 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - MASUK'), 0)

Mobile.getText(findTestObject('Anamnesa/android.widget.TextView0 - Tes Fisik'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - lihat ketentuan'), 0)

Mobile.scrollToText('Form Anamnesa')

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Isi Form Anamnesa'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.TextView0 - Riwayat Periksa Kesehatan'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Ya'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh Klinik Kimia Farma Surabaya'), 'Klinik Tongfang', 
    0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh 12 Desember 2010'), '20 Oktober 2019', 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh Tes kesehatan daftar kerja'), 'Tes Kesehatan', 
    0)

Mobile.tap(findTestObject('Anamnesa/android.widget.RadioButton0 - Ya'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Simpan'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.TextView0 - Riwayat Penyakit'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.ImageButton0'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - Nama penyakit yang pernah diderita'), 'Maag akut', 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Simpan (1)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Lanjut'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Ya (1)'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh Kerang Udang'), 'Ikan Ikanan', 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Lanjut (1)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Ya (2)'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh Termorex paladin'), 'Paratucin', 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Simpan (2)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.TextView0 - Kebiasaan Khusus'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Ya (3)'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh Diet vegetarian'), 'Makan Buah Buahan', 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Lanjut (2)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Ya (4)'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh Jogging gym'), 'Lari Pagi', 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0'), '8', 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.RadioButton0 - Ya (1)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Lanjut (3)'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - Tulis kebiasaanmu yang lain disini'), 'Makan Coklat', 
    0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Lanjut (4)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.TextView0 - Riwayat Lainnya'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Ya (5)'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh Hepatitis B'), 'Tetanus', 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh 10 Oktober 2010'), '20 Januari 2010', 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh Asam mefenamat'), 'Asam Laknat', 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Lanjut (5)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Ya (6)'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 (1)'), '10', 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh 10 Oktober 2010 (1)'), '10 Oktober 2010', 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.RadioButton0 - Ya (2)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Lanjut (6)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Ya (7)'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 (2)'), '5', 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh 10 Oktober 2010 (2)'), '10 Januari 2011', 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh Arak campagne wine'), 'whisky', 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.RadioButton0 - Ya (3)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Lanjut (7)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Ya (8)'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh 10 Oktober 2010 (3)'), '12 Agustus 2018', 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh Tabrakan tertimpa reruntuhan'), 'Patah Hati', 
    0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Lanjut (8)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Ya (9)'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh 10 Oktober 2010 (4)'), '13 Juli 2017', 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - contoh Operasi usus buntu'), 'Operasi Hati', 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Lanjut (9)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.ImageButton0 (1)'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - Nama penyakit yang pernah diderita (1)'), 'Sakit Jantung', 
    0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Simpan (3)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.ImageButton0 (1)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.ImageView0'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.TextView0 - Saudara laki laki'), 0)

Mobile.setText(findTestObject('Anamnesa/android.widget.EditText0 - Nama penyakit yang pernah diderita (1)'), 'beser', 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Simpan (3)'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Simpan (4)'), 0)

Mobile.scrollToText('Kirim')

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Kirim'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.TextView0 - Submit'), 0)

Mobile.tap(findTestObject('Anamnesa/android.widget.Button0 - Konfirmasi Kedatangan'), 0)

Mobile.closeApplication()

